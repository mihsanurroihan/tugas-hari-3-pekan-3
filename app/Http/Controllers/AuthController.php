<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('halaman.daftar');
    }

    public function submit(Request $request)
    {
        //dd($request->all());
        $nama1 = $request["nama1"];
        $nama2 = $request["nama2"];
        $Gender = $request["Gender"];
        $WN = $request["WN"];
        $bio = $request["bio"];

        return view('halaman.selamat', compact('nama1','nama2','Gender','WN','bio'));
    }
}
